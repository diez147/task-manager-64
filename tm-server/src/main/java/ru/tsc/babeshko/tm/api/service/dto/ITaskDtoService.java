package ru.tsc.babeshko.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.dto.model.TaskDto;

import java.util.List;

public interface ITaskDtoService extends IUserOwnedDtoService<TaskDto> {

    @NotNull
    List<TaskDto> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    void removeAllByProjectId(@Nullable String userId, @Nullable String projectId);

    void removeAllByUserId(@Nullable String userId);

}