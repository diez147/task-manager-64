package ru.tsc.babeshko.tm.configuration;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.tsc.babeshko.tm")
public class ApplicationConfiguration {

}