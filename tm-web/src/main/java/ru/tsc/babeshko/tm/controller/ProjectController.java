package ru.tsc.babeshko.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.babeshko.tm.enumerated.Status;
import ru.tsc.babeshko.tm.model.Project;
import ru.tsc.babeshko.tm.repository.ProjectRepository;

import java.util.List;

@Controller
public class ProjectController {

    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    @Nullable
    private List<Project> getProjects() {
        return projectRepository.findAll();
    }

    @NotNull
    @ModelAttribute("statuses")
    private Status[] getStatuses() {
        return Status.values();
    }

    @NotNull
    @GetMapping("/project/create")
    public String create() {
        projectRepository.add("New project" + System.currentTimeMillis());
        return "redirect:/projects";
    }

    @NotNull
    @GetMapping("/project/delete/{id}")
    public String delete(@NotNull @PathVariable("id") final String id) {
        projectRepository.removeById(id);
        return "redirect:/projects";
    }

    @NotNull
    @PostMapping("/project/edit/{id}")
    public String edit(@NotNull @ModelAttribute("project") final Project project, @NotNull final BindingResult result) {
        projectRepository.save(project);
        return "redirect:/projects";
    }

    @NotNull
    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(@NotNull @PathVariable("id") final String id) {
        final Project project = projectRepository.findById(id);
        return new ModelAndView("project-edit", "project", project);
    }

    @NotNull
    @GetMapping("/projects")
    public ModelAndView list() {
        return new ModelAndView("project-list", "projects", getProjects());
    }

}