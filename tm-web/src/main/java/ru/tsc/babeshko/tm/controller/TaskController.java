package ru.tsc.babeshko.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.babeshko.tm.enumerated.Status;
import ru.tsc.babeshko.tm.model.Project;
import ru.tsc.babeshko.tm.model.Task;
import ru.tsc.babeshko.tm.repository.ProjectRepository;
import ru.tsc.babeshko.tm.repository.TaskRepository;

import java.util.List;

@Controller
public class TaskController {

    @NotNull
    @Autowired
    private TaskRepository taskRepository;

    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    @NotNull
    private List<Task> getTasks() {
        return taskRepository.findAll();
    }

    @Nullable
    @ModelAttribute("projects")
    private List<Project> getProjects() {
        return projectRepository.findAll();
    }

    @NotNull
    @ModelAttribute("statuses")
    private Status[] getStatuses() {
        return Status.values();
    }

    @NotNull
    @GetMapping("/task/create")
    public String create() {
        taskRepository.add("new task " + System.currentTimeMillis());
        return "redirect:/tasks";
    }

    @NotNull
    @GetMapping("/task/delete/{id}")
    public String delete(@NotNull @PathVariable("id") final String id) {
        taskRepository.removeById(id);
        return "redirect:/tasks";
    }

    @NotNull
    @PostMapping("/task/edit/{id}")
    public String edit(@NotNull @ModelAttribute("task") final Task task, @NotNull final BindingResult result) {
        if (task.getProjectId().isEmpty() || task.getProjectId() == null) task.setProjectId(null);
        taskRepository.save(task);
        return "redirect:/tasks";
    }

    @NotNull
    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(@NotNull @PathVariable("id") final String id) {
        @NotNull final Task task = taskRepository.findById(id);
        return new ModelAndView("task-edit", "task", task);
    }

    @NotNull
    @GetMapping("/tasks")
    public ModelAndView list() {
        return new ModelAndView("task-list", "tasks", getTasks());
    }

}